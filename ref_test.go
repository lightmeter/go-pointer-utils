// SPDX-FileCopyrightText: 2023 Lightmeter <hello@lightmeter.io>
//
// SPDX-License-Identifier: Apache License 2.0

package ref

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestRefs(t *testing.T) {
	Convey("Refs", t, func() {
		So(RefIfAny(0), ShouldBeNil)
		So(RefIfAny(""), ShouldBeNil)

		So(RefIfAny(2), ShouldResemble, Ref(2))
		So(RefIfAny("hey"), ShouldResemble, Ref("hey"))

		So(Ref(2), ShouldNotBeNil)
		So(Ref(""), ShouldNotBeNil)

		{
			v := Ref("")
			So(v, ShouldNotBeNil)
			So(*v, ShouldEqual, "")
		}

		{
			v := Ref("hey")
			So(v, ShouldNotBeNil)
			So(*v, ShouldEqual, "hey")
		}

		{
			v := RefIfAny("hey")
			So(v, ShouldNotBeNil)
			So(*v, ShouldEqual, "hey")
		}

		{
			v := 34
			So(ValueOrZero(&v), ShouldEqual, 34)
		}

		{
			var v *int
			So(ValueOrZero(v), ShouldEqual, 0)
		}

		{
			v := "hello"
			So(ValueOr(&v, "world"), ShouldEqual, "hello")
			So(ValueOr(nil, "world"), ShouldEqual, "world")
		}
	})
}
