// SPDX-FileCopyrightText: 2023 Lightmeter <hello@lightmeter.io>
//
// SPDX-License-Identifier: Apache License 2.0

package ref

// Return either a pointer points to or a zero value
func ValueOrZero[T any](v *T) T {
	var d T

	return ValueOr(v, d)
}

// Return the value the pointer points too, if any, or f
// This is similar to "coalesce()" on a database
func ValueOr[T any](v *T, f T) T {
	if v != nil {
		return *v
	}

	return f
}

func Ref[T any](v T) *T {
	return &v
}

// Return a pointer to the value in case it is not a zero value
// Return nil otherwise
func RefIfAny[T comparable](v T) *T {
	var zero T

	if v == zero {
		return nil
	}

	return Ref(v)
}
